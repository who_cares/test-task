from __future__ import absolute_import
from celery import Celery
from django.conf import settings

import os


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'km_test.settings')

app = Celery('km_test')

app.config_from_object('km_test.celeryconfig')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
