import os


BASE_DIR = os.path.dirname(os.path.dirname(__file__))

TEMP_FILE_PATH = os.path.join(BASE_DIR, 'tmp')

if not os.path.exists(TEMP_FILE_PATH):
    os.mkdir(TEMP_FILE_PATH)
