from django.test import TestCase

from .models import Client, Poll

from datetime import date, timedelta


class ClientMethodsTests(TestCase):
    """
    Check wheter Age and votes properties behaves well
    """

    def test_age_returns_accurate_amount_of_years(self):
        """
        age property must consider date to count full years passed
        """

        age = 42
        birth_day = date.today()
        birth_day = birth_day.replace(year=birth_day.year - age)
        client = Client(
            first_name='test',
            last_name='test',
            birth_date=birth_day)
        self.assertEqual(client.age, age)
