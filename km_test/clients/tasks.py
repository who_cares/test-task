from __future__ import absolute_import
from celery import shared_task

from django.conf import settings
from .models import Client
from openpyxl import Workbook
from openpyxl.writer.excel import save_virtual_workbook
from datetime import datetime


@shared_task
def create_sheet(file_name='test.xlsx'):
    """
    Create excel file with all clients in it
    """
    book = Workbook(encoding=settings.FILE_CHARSET)
    sheet = book.active
    sheet.title = 'Client list (%s)' % datetime.now().strftime(
        '%d.%m.%y %H.%M')
    column_names = [
        Client._meta.get_field('first_name').verbose_name,
        Client._meta.get_field('last_name').verbose_name,
        Client._meta.get_field('birth_date').verbose_name,
    ]
    sheet.append(column_names)
    clients = Client.objects.all()
    for client in clients:
        sheet.append([client.first_name, client.last_name, client.birth_date])
        cell = sheet.cell(row=sheet.max_row, column=3)
        cell.number_format = 'dd.mm.yyyy'

    book.save(file_name)
    return file_name
