from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.ClientsList.as_view(), name='client_list'),
    url(r'^(?P<pk>\d+)/$', views.ClientDetail.as_view(),
        name='client_detail'),
    url(r'^add/$', views.ClientAdd.as_view(), name='add_client'),
    url(r'^photo_poll/$', views.ClientsVote.as_view(), name='photo_poll'),
    url(r'^download/xlsx_list/$', views.ClientListDownload.as_view(),
        name='download_client_list'),
    url(r'^delete/(?P<pk>\d+)/$', views.ClientRemove.as_view(),
        name='delete_client'),
]
