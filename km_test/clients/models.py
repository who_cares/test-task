from django.db import models

from datetime import date
from uuid import uuid4
import os.path


def uuid_file_names(instance, filename):
    """
    Random name for uploaded files
    """
    file_ext = os.path.splitext(filename)[1]
    file_path = 'clients/photo'
    new_filename = os.path.join(file_path, uuid4().hex + file_ext)
    return new_filename


class Client(models.Model):
    """
    Hypothetical client of Hypothetical company
    """

    first_name = models.CharField(verbose_name='First name', max_length=100)
    last_name = models.CharField(verbose_name='Last name', max_length=100)
    birth_date = models.DateField(verbose_name='Date of birth')
    picture = models.ImageField(verbose_name='Photo',
                                upload_to=uuid_file_names,
                                blank=True)

    @property
    def age(self):
        """
        How many years person lived so far
        """
        now = date.today().timetuple()
        bd = self.birth_date.timetuple()
        lived = now.tm_year - bd.tm_year
        if now.tm_yday < bd.tm_yday:
            lived -= 1
        return lived

    @property
    def votes(self):
        """
        How many votes this client have
        """
        if not hasattr(self, 'poll'):
            return 0
        else:
            return self.poll.votes


class Poll(models.Model):
    """
    Poll for every person in clients
    """
    client = models.OneToOneField(Client,
                                  primary_key=True,
                                  on_delete=models.CASCADE)
    votes = models.IntegerField(verbose_name='Votes', default=0)
