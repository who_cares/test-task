from __future__ import absolute_import
from celery.result import AsyncResult

from django.conf import settings
from django.views import generic
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import SuspiciousOperation
from django.db.models import F
from django.db import IntegrityError, transaction
from django.http import HttpResponseRedirect, Http404, FileResponse, HttpResponseServerError

from .tasks import create_sheet
from .models import Client, Poll

import os.path
from uuid import uuid4


class ClientsList(generic.ListView):
    """
    All of them
    """
    model = Client
    template_name = 'clients/client_list.html'
    context_object_name = 'clients'
    ordering = 'last_name'
    _order_options = ['first_name', 'last_name', 'birth_date',
                      '-first_name', '-last_name', '-birth_date']

    def get_ordering(self):
        """
        Get ordering from request
        """
        ordering = self.request.GET.getlist('order')
        if ordering is not None:
            ordering = [i for i in ordering if i in self._order_options]
        if not ordering:
            ordering = self.ordering
        return ordering

    def get_queryset(self):
        """
        Filter queryset using get request parameters
        """
        queryset = super(ClientsList, self).get_queryset()
        first_name_filter = self.request.GET.get('first_name')
        last_name_filter = self.request.GET.get('last_name')
        if first_name_filter:
            queryset = queryset.filter(first_name__icontains=first_name_filter)
        if last_name_filter:
            queryset = queryset.filter(last_name__icontains=last_name_filter)
        return queryset

    def get_context_data(self, **kwargs):
        """
        Keep filter throught subsequent requests
        """
        context = super(ClientsList, self).get_context_data(**kwargs)
        context['first_name'] = self.request.GET.get('first_name', '')
        context['last_name'] = self.request.GET.get('last_name', '')
        return context


class ClientDetail(generic.DetailView):
    """
    All we know about it
    """
    model = Client
    template_name = 'clients/client_detail.html'
    context_object_name = 'client'


class ClientAdd(generic.CreateView):
    """
    Make a new one
    """
    model = Client
    template_name_suffix = '_add'
    fields = ['first_name', 'last_name', 'birth_date', 'picture']
    success_url = reverse_lazy('clients:client_list')


class ClientRemove(generic.DeleteView):
    """
    Get rid of some
    """
    model = Client
    template_name_suffix = '_delete'
    context_object_name = 'client'
    success_url = reverse_lazy('clients:client_list')


class ClientListDownload(generic.TemplateView):
    """
    All in one sheet
    """
    template_name = 'clients/download_client_list.html'
    tmp_file_path = settings.TEMP_FILE_PATH
    mime_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    downloaded_file_name = 'Client_list.xlsx'
    task = None

    def get_tmp_file_name(self):
        """
        UUID as a temporary file name
        """
        return os.path.join(self.tmp_file_path, uuid4().hex)

    def get(self, request, *args, **kwargs):
        """
        Generate new file and create new link to it
        """
        task_id = request.GET.get('download_id')
        if not task_id:
            self.task = create_sheet.delay(self.get_tmp_file_name()).id
        else:
            result = AsyncResult(task_id)
            if result.failed():
                return HttpResponseServerError('Some error occured')
            if result.ready():
                response = FileResponse(
                    open(result.result, 'rb'),
                    content_type=self.mime_type)
                response['Content-Disposition'] = (
                    'attachment; filename=%s' % self.downloaded_file_name)
                return response
            else:
                self.task = task_id

        return super(ClientListDownload, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """
        Add link to file
        """
        context = super(ClientListDownload, self).get_context_data(**kwargs)
        if self.task:
            context['file_id'] = self.task
        return context


class ClientsVote(generic.ListView):
    """
    Likes
    """
    template_name = 'clients/photo_poll.html'
    context_object_name = 'photo_list'
    queryset = Client.objects.exclude(picture='')
    http_method_names = ['get', 'post']
    _max_likes = 10

    @transaction.atomic
    def post(self, request):
        """
        Add like to a photo
        """
        photo_id = request.POST.get('vote')

        try:
            photo_id = int(photo_id)
        except (ValueError, TypeError):
            raise SuspiciousOperation

        try:
            client = Client.objects.get(id=photo_id)
        except Client.DoesNotExist:
            raise Http404('No such photo')

        if not hasattr(client, 'poll'):
            try:
                Poll(client=client).save(force_insert=True)
            except IntegrityError:
                pass

        Poll.objects.filter(
            client=client,
            votes__lt=self._max_likes
        ).select_for_update().update(votes=F('votes') + 1)
        return HttpResponseRedirect(reverse_lazy('clients:photo_poll'))

    def get_context_data(self, **kwargs):
        """
        Add likes limit to template
        """
        context = super(ClientsVote, self).get_context_data(**kwargs)
        context['max_likes'] = self._max_likes
        return context
